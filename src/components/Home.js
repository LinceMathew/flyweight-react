import React, {useState, useEffect} from 'react'
import axios from 'axios'
import ReactPaginate from 'react-paginate';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { flexbox } from '@material-ui/system';
import '../CssModules/Pagination.css';


export default function Home() {


  const useStyles = makeStyles((theme) => ({
 
  root: {
    
    display: 'flex',
    '& > *': {
      margin: theme.spacing(2),
      width: theme.spacing(37),
      height: theme.spacing(25),
    },
  },
  grid: {
    
    
      margin: theme.spacing(1),
      
    
  },
  paper:{
    background:"#a6c5ff",
  }

  }));


  const [currentPage, setCurrentPage] = useState(1);//index of the page
  const[Data,setData] =useState([]);//here we store the ten data
  const classes = useStyles();
  const[User,setUser] =useState([]);
  const[count,setCount]=useState(0);


  useEffect(() => {
  getData(currentPage)
  }, [])

function handlePageClick({
 selected:selectedPage }) {
setCurrentPage(selectedPage);
getData(selectedPage+1)
}

      const  getData = async function getdata(pageNumber){
      const url =`http://127.0.0.1:8000/v1/sessions/?page=${pageNumber}`;
      const res = await axios.get(`${url}`)
      const SessionData = res.data.data.sessions;
      const Count=res.data.meta.count;     
                setData(SessionData)
                setCount(Count)
                
                
  }
  
  const PER_PAGE =10;// 10 data per page
  const offset = ((currentPage-1)*PER_PAGE); //NumberOfItemsThatHaveBeenAlreadyDisplayedBySytem
  
  const currentPageData =Data
                          
                          .map(pd=> 
                           
                          <div>
                          <h2>{pd.name}</h2> 


                                                               
                                <Grid  container sm={12} className={classes.root} >
                                
                                 
                               
                                
                                {
                                  pd.users.map(user =>
                                  {
                                    return(
                                      <Paper className={classes.paper} >
                                        <p className={"user"}>{user.name}</p>
                                        <Grid container sm={12}>  
                                        
                                        {
                                           
                                          
                                        
                                          
                                            user.thumbnails?.map(thumb=>
                                            {
                                              
                                              
                                                return(
                                                  <>
                                                    <img src={thumb.link} width="90" height="90" ></img>                                  
                                                      
                                                    
                                                  </>
                                                        )
                                                                                                                                                              

                                                       
                                            }
                                            
                                                               )
                                                                                                                             
                                                                                                                      
                                          
                                        } 
                                         
                                          </Grid>
                                           <p className={"thumb"}>{user.thumbnails?.[0].windowTitle}</p> 
                                      </Paper>

                                            )
                                  }
                                                  )
                                }
                                        
                                
                                </Grid>
                                           
                          </div>
                            );

                          const PageCount =count;
 return(

 		<div>
      <p>{console.log(PageCount)}</p>
      <p>{currentPageData}</p>

          <br/>
          <br/>
         <ReactPaginate
        previousLabel={"Previous"}
        nextLabel={"Next"}
        pageCount={PageCount}
        onPageChange={handlePageClick}
        containerClassName={"pagination"}
        previousLinkClassName={"pagination__link"}
        nextLinkClassName={"pagination__link"}
        disabledClassName={"pagination__link--disabled"}
        activeClassName={"pagination__link--active"}
      />

    </div>
  );
}

