import React from 'react';
import {Link} from 'react-router-dom';
import styles from '../CssModules/NavBar.module.css';
class NavBar extends React.Component {

	render() {
		return(
			<div className ={styles.spacer}>
			<div className = {styles.navbar}>
			<p className = {styles.brand}> IMPRESSION RIVER</p>
			<button className = {styles.navbtn}>timeStamp picker</button>
			<button className = {styles.navbtn}>Filter</button>
	
			</div>
			</div>	

			);
	}
}

export default NavBar;
