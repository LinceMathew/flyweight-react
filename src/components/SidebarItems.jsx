const SidebarItems = [
    {
        name: "Sessions",
        route: '/'
    },
    
    {
        name: "Group",
        route: '/page-1'
    },
    {
        name: "User",
        route: '/page-2'
    },
    {
        name: "Settings",
        route: 'page-3'
    },
];

export default SidebarItems;