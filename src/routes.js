import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Page1 from "./pages/page-1";
import Page2 from "./pages/page-2";
import Page3 from "./pages/page-3";
import Dashboard from "./components/Dashboard";
import Home from './components/Home';
import App from "./App";

function Routes() {
    return (
        <BrowserRouter>
            <Route render={(props)=>(
                <App {...props}>
                    <Switch>
                        <Route path="/" exact component={Dashboard}/>
                        <Route path="/Page-1" component={Page1}/>
                        <Route path="/Page-2" component={Page2}/>
                        <Route path="/Page-3" component={Page3}/>
                       
                    </Switch>
                </App>
            )}/>
        </BrowserRouter>
    )
}

export default Routes;