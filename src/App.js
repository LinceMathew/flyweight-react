import React from 'react';
import Routes from "./routes";
import Sidebar from "./components/Sidebar";
import NavBar from "./components/NavBar";

function App(props) {
    return (
        <div>
            <div style={{display: "flex"}}>
                <Sidebar history={props.history}/>
                <div style={{maxWidth: '800px'}}>
                     <NavBar/>
                    {props.children}
                </div>
            </div>
        </div>
    );
}

export default App;